+++
Type = "Project"
+++

# Exert

This is a project I worked on when I was at a start up.  That
was a really good learning experience.  Unfortunately (or maybe
fortunately) we ran out of run way before we could get the next
round of funding or before we could bootstrap the business.
However, it was my first pass at writing an app and was a good
learning experience for me.

Exert was a real time chat group fitness competition app.  When
you joined the app you where put in a team of 4 other people and
pitted against another team of 5 people.  So you need 10 people
to make it work.  You had a best out of 7 day battle against the
other team to get more points than them each day.  You had to
work with your other teammates to earn those points.  Throughout
the day each person received fitness challenges.  People had to
do the challenges to earn points. You also received extra points
for posting pictures and videos.  It was an interesting concept,
but I'm not sure what the plan was to monetize it.  However, I want to
talk about the tech side of it and what I learned there.

I build this around 2015, 2016 - right about the time when the
hype for NoSQL was at it's peak.  So being a good trend follow,
we picked MongoDB as our back end, Node.js as the server tech,
and Ionic as the front end tech.  It was a real time app, but
after evaluation all the options I chose to use socket.io.  The
previous project our team used sails.js.  I found that way to
opinionated, and for this projected, opted for express.js and
mongoose as the ORM.  Waterline was a great abstraction over SQL
and NoSQL, but because it abstracted over both, it was fairly
limited.  At the time you couldn't do nested associates, or have
a schema for nested JSON objects in Mongo - so any of those
became a black hole that anything could go in.  So mongoose
solved that problem, and express + socket.io was flexible enough
to do what we needed.  Through this I found that I prefer
simplicity and control over ease.  Sails was easy until you
needed it to do something it couldn't.  Then it got really
hairy.  Using plain express with socket.io gave me a level of
control that I couldn't get with Sails.js.

Over time I realized that using a SQL database would have been a
way better choice.  We wanted reporting on all our data.  Mongo
at the time couldn't do joins.  So we replicated all the mongo
data into a MySQL database for reporting.  I tailed the oplog of
mongo and dumped the data as I got it into mongo.  Not only was
the reporting bad, but we had to have a lot of application code
for something that could have easily been expressed in an SQL
query.  So with these battles each team member accumulated
points and each team had points total for the day to compare to
the other teams points.  What I ended up doing what having a
points log, but whenever data was inserted in the points log I
had to update a cache total points for the person for the day
and the total team for the day.  Mongo couldn't easily do it,
not to mention it's aggregation pipeline at the time was a pain
to use.  Compare that to just doing a few joins and group bys in
an SQL query.

It was a fun project.  I learned a lot.  It has really helped me
make better architectural decisions in the future.  The start up
morphed into a consultancy and we still have the code base
sitting around.
