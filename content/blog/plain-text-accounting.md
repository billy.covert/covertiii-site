---
Type: "Blog"
Description: "Envelope system using ledger cli."
---

# My Plain Text Accounting Work Flow

To keep track of my personal finances I use hledger.  I've been
using it for almost 2 years now and it works great.  Why use
a command line tool?  I work as a developer so I'm completely
comfortable using the command line.  The reports I get with
hledger are better than any tool I've ever used.  I also like
the idea being in control of my data - it's plain text.  I stick
to using features that can be compatible with other ledger tools
as well, like ledger.  Since the data format is a simple text
file, I version it using git.

What are the drawbacks?  Well, my wife doesn't know how to use
it.  To get around that, I print off reports and we review
them together.  Now, I run a balance command, take a screenshot,
and send it to her via iMessage.   If we need to try down, I run
hledger web and run the register report to see how balances of
things changed over time.

So as far as my work flow goes, I use an envelope based system. I
have the normal 5 accounts (Assets, Income, Expenses, Liability,
and Equity) plus I have a new one, Buckets.  Buckets has it's
own 5 sub accounts (Assets, Income, Expenses,  Liabilities, and
Equity), that mirror the main accounts.  Within Buckets:Assets I
have my envelopes.  The total of Buckets:Assets must be the same
as Assets.  I haven't found a way of enforcing this, so after I
update my journal file, I run a balance command and check.

Whenever I update my journal file each real transaction has a
shadow transaction with it that moves money in the fake buckets
account.  For example:

```
2020-01-10 CKCD DEBIT 01/09 AMAZON.COM SEATTLE WA
  Assets:Checking  $-58.25
  Expenses:Pet  $58.25
  Buckets:Assets:Pet  $-58.25
  Buckets:Expenses
```

The buckets accounts allow me to maintain balances for specific
sub accounts.  The
normal expenses account allows me to check how much per month we
are spending in any given subaccounts.

## Weekly Habits

I try to update my journal every week.  It usually works out to
every two weeks.  To update my journal, I download a csv file
from my bank.  I look at the last transaction in my journal file
and delete any transactions already entered in the journal file
from the csv file.  I wrote a node.js script that categorizes
everything.  I run the conversion once and look it over.  If
there's anything new that new needs categorized, I update the
script and rerun it.  That usually looks like `node
scripts/index.js csv/2020-01-22-checkings.csv`  It outputs to
stdout.  On a mac I pipe that to my clipboard once it looks good
and append it to my journal.  I update any hard to categorize
accounts in the journal file by hand.  When I'm done, I run a
balance report to see if `Assets` matches `Bucket:Assets`, and
that my `Assets` match what's really in my bank account.  Once
I'm satisfied with that, I run a balance report on
`Bucket:Assets` to see where those accounts stand.  I also run a
balance report for the month on `Expenses` to see where all my
money has gone.  If something looks off in `Buckets:Assets` I
run a registry report on the specific sub account to see what
might have thrown it off.  Once I'm satisfied my wife and I
review the bucket balances together and drill in specific sub accounts using the registry command.

## Monthly Habits

Any income I get goes in `Buckets:Assets:Income`. At the
beginning of every month, I take the money from
`Buckets:Assets:Income` and distribute it to the other sub
accounts.  Untils it's balance is 0 or if I know next month I'll
be short and I need to reserve funds.  Having been self employed
for the past 5 years, my income can fluctuate a lot from month
to month. That looks something like this:

```
2020-01-01 Monthly Fill
  Buckets:Assets:Auto:Gas  $50
  Buckets:Assets:Income
```

That takes money from the Income account and puts it in the Gas
account.  I run my weekly reports on the previous month to get a
snapshot of that, and then I run those reports on the current
month.  Usually we overspend in a particular area.  To fix that,
there's a `Buckets:Assets:Savings` account where that money gets
pulled from to compensate.

## Conclusion

The more frequently I update my journal, the more in control I
feel of my finanaces.  When I update the journal less
frequently, the household spending can get out of control.
However, out of any tool I've used I feel like hledger puts me
in control of my finances.

My work flow isn't perfect, but it works for me.  I think there
could be a better way implement the envolope system, but what I
do works for me so far.  In the future I may investigate other
possibilities.  I think things I can do better is try to my the
journal updating process easier and less painful.  The worst
part for me is categorizing our Amazon transactions.  The auto
categorizing is a step in the right direction but I'm not sure
if automating it helps.  Looking though each transaction to make
sure it's categorized correctly forces me to think about where
my money is going and if it should be going there.

The other thing is it would be great if there was a tool that
made it easy for my wife to have a look in our finances.
`hledger-web` is a step in the right direction, but requires a
dedicated server running plus all the security implications that
brings.  What would be ideal is a mobile client that run hledger
or ledger under the hood, that uses git as a backend to retrieve
data.

