+++
Description = ""
Tags = []
Categories = []
Type = "Page"
+++

# Hey, I'm Bill

I like to build stuff, tinker and learn about new tech.

## Blog

Ramblings about different tech topics.

 - [Plain Text Accounting](/blog/plain-text-accounting)

## Projects

Here's different projects I've worked on through the years and
the things I've learned along the way.

 - [Plotting Program](https://s5plot-web.herokuapp.com/)
 - [Exert](/projects/exert)
 - [Real World API Server](https://github.com/CovertIII/realworld-express-sequelize/)

## Contact

You can email me at bill at this domain.  I'd love to chat.
